import gym
import numpy as np
import tensorflow as tf

from matplotlib import pyplot as plt
from collections import deque
import tqdm
import statistics

print(tf.executing_eagerly())

env = gym.make('CartPole-v0')

class ActorCritic(tf.keras.Model):
    def __init__(self, num_actions):
        super().__init__()
        self.dense0 = tf.keras.layers.Dense(128, activation='relu')
        self.actor = tf.keras.layers.Dense(num_actions)
        self.critic = tf.keras.layers.Dense(1)

    def call(self, x):
        x = self.dense0(x)
        return self.actor(x), self.critic(x)

model = ActorCritic(env.action_space.n)


def env_step(action):
    state, reward, done, info = env.step(action)
    return np.array(state, dtype=np.float32), np.array(reward, dtype=np.int32), done

def tf_env_step(action):
    return tf.numpy_function(env_step, [action],  [tf.float32, tf.int32, tf.bool])


def run_ep(initial_state, model, max_steps, render=False):
    action_probss = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
    critic_values = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
    rewards = tf.TensorArray(dtype=tf.int32, size=0, dynamic_size=True)

    state = initial_state

    for t in range(max_steps):
        state = tf.expand_dims(state, 0)
        actions, critic_value = model(state)

        if render:
            env.render()

        action = tf.random.categorical(actions, 1)[0, 0]
        action_probs = tf.nn.softmax(actions)

        critic_values = critic_values.write(t, tf.squeeze(critic_value))
        action_probss = action_probss.write(t, action_probs[0, action])

        state, reward, done = tf_env_step(action)

        rewards = rewards.write(t, reward)

        if done:
            break

    action_probss = action_probss.stack()
    critic_values = critic_values.stack()
    rewards = rewards.stack()

    return action_probss, critic_values, rewards

def get_expected_return(rewards, gamma, standardize=True):
    n = tf.shape(rewards)[0]
    returns = tf.TensorArray(dtype=tf.float32, size=n)
    rewards = tf.cast(rewards[::-1], dtype=tf.float32)
    discounted_sum = tf.constant(0.0)
    for i in tf.range(n):
        reward = rewards[i]
        discounted_sum = reward + gamma * discounted_sum
        returns = returns.write(i, discounted_sum)
    returns = returns.stack()[::-1]
    if standardize:
        returns = ((returns - tf.math.reduce_mean(returns)) / (tf.math.reduce_std(returns)))

    return returns

huber_loss = tf.keras.losses.Huber(reduction=tf.keras.losses.Reduction.SUM)

def compute_loss(action_probss, critic_values, returns):
    advantage = returns - critic_values
    action_log_probs = tf.math.log(action_probss)

    actor_loss = -tf.math.reduce_sum(action_log_probs * advantage)

    critic_loss = huber_loss(critic_values, returns)

    return actor_loss + critic_loss

optimizer = tf.keras.optimizers.Adam(learning_rate=0.01)

def train_step(initial_state, model, optimizer, gamma, max_steps):
    with tf.GradientTape() as tape:
        action_probss, critic_values, rewards = run_ep(initial_state, model, max_steps) 
        returns = get_expected_return(rewards, gamma)
        action_probss, critic_values, returns = [tf.expand_dims(x, 1) for x in [action_probss, critic_values, returns]] 
        loss = compute_loss(action_probss, critic_values, returns)
    
    grads = tape.gradient(loss, model.trainable_variables)
    optimizer.apply_gradients(zip(grads, model.trainable_variables))
    episode_reward = tf.math.reduce_sum(rewards)
    return episode_reward

min_episodes_criterion = 100
max_episodes = 1000
max_steps_per_episode = 1000

reward_threshold = 150

gamma = 0.99

episodes_reward = deque(maxlen=min_episodes_criterion)

with tqdm.trange(max_episodes) as t:
    for i in t:
        initial_state = tf.constant(env.reset(), dtype=tf.float32)
        episode_reward = int(train_step(initial_state, model, optimizer, gamma, max_steps_per_episode))
        episodes_reward.append(episode_reward)

        t.set_description(f'Episode {i}')
        running_reward = statistics.mean(episodes_reward)
        t.set_postfix(episode_reward=episode_reward, running_reward=running_reward)
        if running_reward > reward_threshold and i >= min_episodes_criterion:  
            break

run_ep(tf.constant(env.reset(), dtype=tf.float32), model, 1000, True)


